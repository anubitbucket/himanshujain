package com.investor.project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.management.InvalidAttributeValueException;

/**
 * 
 * @author User
 *
 */
public class VertexHelper {

	private Map<Integer, Vertex> investors = new HashMap<>();
	private Map<Integer, Vertex> funds = new HashMap<>();
	private Map<Integer, Vertex> holdings = new HashMap<>();
	private int investorId = 0;
	private int fundId = 0;
	private int holdingId = 0;

	/**
	 * This method creates investor and provide unique id
	 * 
	 * @param investorName
	 * @throws InvalidAttributeValueException
	 */
	public void addInvestor(String investorName) throws InvalidAttributeValueException {
		if (investorName.isEmpty())
			throw new InvalidAttributeValueException("Investor Name cannot be empty");
		investorId++;
		Vertex investor = new Vertex(investorName, investorId);
		investors.put(investorId, investor);
		System.out.println("Investor Id is " + investorId);
	}

	/**
	 * This method creates fund and provide unique id
	 * 
	 * @param fundName
	 * @throws InvalidAttributeValueException
	 */
	public void addFund(String fundName) throws InvalidAttributeValueException {
		if (fundName.isEmpty())
			throw new InvalidAttributeValueException("Fund Name cannot be empty");
		fundId++;
		Vertex fund = new Vertex(fundName, fundId);
		funds.put(fundId, fund);
		System.out.println("Fund Id is " + fundId);
	}

	/**
	 * This method creates holding and provide unique id
	 * 
	 * @param holdingName
	 * @param holdingValue
	 * @throws InvalidAttributeValueException
	 */
	public void addHolding(String holdingName, int holdingValue) throws InvalidAttributeValueException {
		if (holdingName.isEmpty())
			throw new InvalidAttributeValueException("Holding Name cannot be empty");
		holdingId++;
		Vertex holding = new Vertex(holdingName, holdingId, holdingValue);
		holdings.put(holdingId, holding);
		System.out.println("Holding Id is " + holdingId);
	}

	/**
	 * This method performs following steps: 1. Creates mapping between Investor and
	 * Fund. If Investor or Fund is not available then it creates them 2. Creates
	 * mapping between Fund and Holding. If Fund or Holding is not available then it
	 * creates them 3. Quantity is applicable when we are creating mapping between
	 * Fund and Holding. 4. Level value should be 1 in order to create mapping
	 * between Investor and Fund. 5. Level value should be 2 in order to create
	 * mapping between Fund and Holding.
	 * 
	 * @param parentNodeName
	 * @param childNodeName
	 * @param level
	 * @param quantity
	 * @throws InvalidAttributeValueException
	 */
	public void addMappingByName(String parentNodeName, String childNodeName, int level, int quantity)
			throws InvalidAttributeValueException {

		// Throwing error if parentNodeName or childNodeName is empty
		if (parentNodeName.isEmpty() || childNodeName.isEmpty())
			throw new InvalidAttributeValueException("Parent Node Name or Child Node Name cannot be empty");

		// Creating mapping between Investor and Fund if level is 1
		if (level == 1) {

			// Checking if Investor is already present or not. If not then we will create
			// new Investor with provided name
			Optional<Vertex> investorOptional = investors.values().stream()
					.filter(f -> f.getName().equals(parentNodeName)).findFirst();
			Vertex investor = !investorOptional.isPresent() ? new Vertex(parentNodeName, investorId++)
					: investorOptional.get();
			investors.put(investor.getId(), investor);

			// Checking if Fund is already present or not. If not then we will create new
			// Fund with provided name
			Optional<Vertex> fundOptional = funds.values().stream().filter(f -> f.getName().equals(childNodeName))
					.findFirst();
			Vertex fund = !fundOptional.isPresent() ? new Vertex(childNodeName, fundId++) : fundOptional.get();
			funds.put(fund.getId(), fund);

			investor.getEdges().put(fund, 0);

			System.out.println("Investor Id is " + investor.getId() + " && Fund Id is " + fund.getId());

		}
		// Creating mapping between Fund and Holding if level is 2
		else if (level == 2) {

			// Checking if Fund is already present or not. If not then we will create new
			// Fund with provided name
			Optional<Vertex> fundOptional = funds.values().stream().filter(f -> f.getName().equals(parentNodeName))
					.findFirst();
			Vertex fund = !fundOptional.isPresent() ? new Vertex(parentNodeName, fundId++) : fundOptional.get();
			funds.put(fund.getId(), fund);

			// Checking if Holding is already present or not. If not then we will create new
			// Holding with provided name
			Optional<Vertex> holdingOptional = holdings.values().stream().filter(f -> f.getName().equals(childNodeName))
					.findFirst();
			Vertex holding = !holdingOptional.isPresent() ? new Vertex(childNodeName, holdingId++)
					: holdingOptional.get();
			holdings.put(holding.getId(), holding);

			fund.getEdges().put(holding, quantity);

			System.out.println("Fund Id is " + fund.getId() + " && Holding Id is " + holding.getId());

		} else {
			throw new InvalidAttributeValueException("Value of level attribute can only be either 1 or 2.");
		}
	}

	/**
	 * This method determines and return market value of an Investor. It excludes
	 * the Holding during calculation which are passed as second parameter.
	 * 
	 * @param investorId
	 * @param excludeHoldingList
	 * @return market value
	 * @throws InvalidAttributeValueException
	 */
	public int getMarketValueByInvestor(int investorId, List<Integer> excludeHoldingList)
			throws InvalidAttributeValueException {
		if (!investors.containsKey(investorId))
			throw new InvalidAttributeValueException("Investor with investor id " + investorId + " is not present.");
		final List<Integer> holdingList = excludeHoldingList == null ? new ArrayList<>() : excludeHoldingList;
		int marketValue = investors.get(investorId).getEdges().keySet().stream().map(k -> {
			int total = 0;
			for (Map.Entry<Vertex, Integer> e : k.getEdges().entrySet()) {
				if (!holdingList.contains(e.getKey().getId())) {
					total += e.getKey().getHoldingValue() * e.getValue();
				}
			}
			return total;
		}).reduce(0, (m, n) -> m + n);
		return marketValue;
	}

	/**
	 * This method determines and return market value of a Fund. It excludes the
	 * Holding during calculation which are passed as second parameter.
	 * 
	 * @param fundId
	 * @param excludeHoldingList
	 * @return market value
	 * @throws InvalidAttributeValueException
	 */
	public int getMarketValueByFund(int fundId, List<Integer> excludeHoldingList)
			throws InvalidAttributeValueException {
		if (!funds.containsKey(fundId))
			throw new InvalidAttributeValueException("fund with fund id " + fundId + " is not present.");
		final List<Integer> holdingList = excludeHoldingList == null ? new ArrayList<>() : excludeHoldingList;
		int marketValue = funds.get(fundId).getEdges().entrySet().stream().map(e -> {
			int total = 0;
			if (!holdingList.contains(e.getKey().getId())) {
				total += e.getKey().getHoldingValue() * e.getValue();
			}
			return total;
		}).reduce(0, (m, n) -> m + n);
		return marketValue;
	}

	/**
	 * This method prints the details of an Investor
	 * 
	 * @param id
	 */
	public void printInvestorDetails(int id) {

		if (investors.containsKey(id)) {
			Vertex v = investors.get(id);
			System.out.println(v.getName());
			v.getEdges().keySet().stream().forEach(e -> {
				System.out.print(e.getName() + " --> ");
				e.getEdges().keySet().stream().forEach(e1 -> System.out.print(e1.getName() + ", "));
				System.out.println();
			});
		} else {
			System.out.println("Investor details are not present for investor id " + id);
		}
	}

	private class Vertex {
		private String name;
		private final int id;
		private Map<Vertex, Integer> edges = new HashMap<Vertex, Integer>();
		private int holdingValue;

		public Vertex(String name, int id) {
			this.name = name;
			this.id = id;
		}

		public Vertex(String name, int id, int holdingValue) {
			this.name = name;
			this.id = id;
			this.holdingValue = holdingValue;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Map<Vertex, Integer> getEdges() {
			return edges;
		}

		public int getHoldingValue() {
			return holdingValue;
		}

		public void setHoldingValue(int holdingValue) {
			this.holdingValue = holdingValue;
		}

		public int getId() {
			return id;
		}

		@Override
		public int hashCode() {
			return id;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null || obj.getClass() != this.getClass())
				return false;
			Vertex i = (Vertex) obj;
			return id == i.getId();
		}
	}
}
