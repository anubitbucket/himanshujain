package com.investor.project;

import javax.management.InvalidAttributeValueException;

public class Main {

	public static void main(String[] args) throws InvalidAttributeValueException {
		VertexHelper helper = new VertexHelper();

		// Adding Investors by passing Investor Name
		helper.addInvestor("Inv1");
		helper.addInvestor("Inv2");

		// Adding Funds by passing Fund Name
		helper.addFund("F1");
		helper.addFund("F2");
		helper.addFund("F3");

		// Adding Holdings by passing Holding Name and Holding value
		helper.addHolding("H1", 10);
		helper.addHolding("H2", 20);
		helper.addHolding("H3", 15);
		helper.addHolding("H4", 10);

		// Adding Investor and Fund mapping by passing Investor Name, Fund Name, Level
		// and Quantity (NA in this case)
		helper.addMappingByName("Inv1", "F1", 1, 0);
		helper.addMappingByName("Inv1", "F2", 1, 0);

		helper.addMappingByName("Inv2", "F2", 1, 0);
		helper.addMappingByName("Inv2", "F3", 1, 0);

		// Adding Fund and Holding mapping by passing Fund Name, Holding Name, Level and
		// Quantity
		helper.addMappingByName("F1", "H1", 2, 100);
		helper.addMappingByName("F1", "H2", 2, 100);
		helper.addMappingByName("F1", "H4", 2, 100);

		helper.addMappingByName("F2", "H1", 2, 100);
		helper.addMappingByName("F2", "H3", 2, 100);

		helper.addMappingByName("F3", "H1", 2, 100);
		helper.addMappingByName("F3", "H4", 2, 100);

		// Printing investor details by passing Investor Id
		helper.printInvestorDetails(1);
		helper.printInvestorDetails(2);

		// Calculating Market Value by passing Investor Id and list of Holdings that
		// need to exclude
		System.out.println(helper.getMarketValueByInvestor(1, null));
		System.out.println(helper.getMarketValueByInvestor(2, null));

		// Calculating Market Value by passing Fund Id and list of Holdings that need to
		// exclude
		System.out.println(helper.getMarketValueByFund(1, null));
		System.out.println(helper.getMarketValueByFund(2, null));
	}

}
