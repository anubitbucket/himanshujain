package com.investor.project;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import javax.management.InvalidAttributeValueException;

import org.junit.Test;

public class VertexHelperTest {

	private VertexHelper helper = new VertexHelper();

	@Test
	public void createInvestor_basic_test() {
		assertDoesNotThrow(() -> helper.addInvestor("Inv1"));
	}

	@Test
	public void createInvestor_exception_test() {
		assertThrows(InvalidAttributeValueException.class, () -> helper.addInvestor(""));
	}

	@Test
	public void createFund_basic_test() {
		assertDoesNotThrow(() -> helper.addFund("F1"));
	}

	@Test
	public void createFund_exception_test() {
		assertThrows(InvalidAttributeValueException.class, () -> helper.addFund(""));
	}

	@Test
	public void createHolding_basic_test() {
		assertDoesNotThrow(() -> helper.addHolding("H1", 10));
	}

	@Test
	public void createHolding_exception_test() {
		assertThrows(InvalidAttributeValueException.class, () -> helper.addHolding("", 10));
	}

	@Test
	public void addMappingByName_exception_test() {
		assertThrows(InvalidAttributeValueException.class, () -> helper.addMappingByName("", "", 0, 0));
	}

	@Test
	public void addMappingByName_incorrect_level_value_exception_test() {
		assertThrows(InvalidAttributeValueException.class, () -> helper.addMappingByName("Inv1", "F1", 0, 0));
	}

	@Test
	public void addMappingByName_basic_test_with_level_1() {
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F1", 1, 0));
	}

	@Test
	public void addMappingByName_basic_test_with_level_2() {
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H1", 2, 0));
	}

	@Test
	public void addMappingByName_basic_test_with_existing_investor_and_fund() {
		// Adding Investors by passing Investor Name
		assertDoesNotThrow(() -> helper.addInvestor("Inv1"));

		// Adding Funds by passing Fund Name
		assertDoesNotThrow(() -> helper.addFund("F1"));
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F1", 1, 0));
	}

	@Test
	public void addMappingByName_basic_test_with_existing_fund_and_holding() {

		// Adding Funds by passing Fund Name
		assertDoesNotThrow(() -> helper.addFund("F1"));

		// Adding Holdings by passing Holding Name and Holding value
		assertDoesNotThrow(() -> helper.addHolding("H1", 10));

		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H1", 2, 0));
	}

	@Test
	public void getMarketValueByInvestor_basic_test() throws InvalidAttributeValueException {
		// Adding Investors by passing Investor Name
		assertDoesNotThrow(() -> helper.addInvestor("Inv1"));

		// Adding Funds by passing Fund Name
		assertDoesNotThrow(() -> helper.addFund("F1"));
		assertDoesNotThrow(() -> helper.addFund("F2"));

		// Adding Holdings by passing Holding Name and Holding value
		assertDoesNotThrow(() -> helper.addHolding("H1", 10));
		assertDoesNotThrow(() -> helper.addHolding("H2", 20));
		assertDoesNotThrow(() -> helper.addHolding("H3", 15));
		assertDoesNotThrow(() -> helper.addHolding("H4", 10));

		// Adding Investor and Fund mapping by passing Investor Name, Fund Name, Level
		// and Quantity (NA in this case)
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F1", 1, 0));
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F2", 1, 0));

		// Adding Fund and Holding mapping by passing Fund Name, Holding Name, Level and
		// Quantity
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H2", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H4", 2, 100));

		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H3", 2, 100));

		assertEquals(6500, helper.getMarketValueByInvestor(1, null));
	}

	@Test
	public void getMarketValueByInvestor_basic_test_with_excludeHoldingList() throws InvalidAttributeValueException {
		// Adding Investors by passing Investor Name
		assertDoesNotThrow(() -> helper.addInvestor("Inv1"));

		// Adding Funds by passing Fund Name
		assertDoesNotThrow(() -> helper.addFund("F1"));
		assertDoesNotThrow(() -> helper.addFund("F2"));

		// Adding Holdings by passing Holding Name and Holding value
		assertDoesNotThrow(() -> helper.addHolding("H1", 10));
		assertDoesNotThrow(() -> helper.addHolding("H2", 20));
		assertDoesNotThrow(() -> helper.addHolding("H3", 15));
		assertDoesNotThrow(() -> helper.addHolding("H4", 10));

		// Adding Investor and Fund mapping by passing Investor Name, Fund Name, Level
		// and Quantity (NA in this case)
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F1", 1, 0));
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F2", 1, 0));

		// Adding Fund and Holding mapping by passing Fund Name, Holding Name, Level and
		// Quantity
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H2", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H4", 2, 100));

		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H3", 2, 100));

		List<Integer> list = new ArrayList<>();
		list.add(1);

		assertEquals(4500, helper.getMarketValueByInvestor(1, list));

	}

	@Test
	public void getMarketValueByInvestor_basic_test_with_incorrect_investor_id() throws InvalidAttributeValueException {
		assertThrows(InvalidAttributeValueException.class, () -> helper.getMarketValueByInvestor(10, null));
	}

	@Test
	public void getMarketValueByFund_basic_test() throws InvalidAttributeValueException {
		// Adding Investors by passing Investor Name
		assertDoesNotThrow(() -> helper.addInvestor("Inv1"));

		// Adding Funds by passing Fund Name
		assertDoesNotThrow(() -> helper.addFund("F1"));
		assertDoesNotThrow(() -> helper.addFund("F2"));

		// Adding Holdings by passing Holding Name and Holding value
		assertDoesNotThrow(() -> helper.addHolding("H1", 10));
		assertDoesNotThrow(() -> helper.addHolding("H2", 20));
		assertDoesNotThrow(() -> helper.addHolding("H3", 15));
		assertDoesNotThrow(() -> helper.addHolding("H4", 10));

		// Adding Investor and Fund mapping by passing Investor Name, Fund Name, Level
		// and Quantity (NA in this case)
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F1", 1, 0));
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F2", 1, 0));

		// Adding Fund and Holding mapping by passing Fund Name, Holding Name, Level and
		// Quantity
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H2", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H4", 2, 100));

		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H3", 2, 100));

		assertEquals(4000, helper.getMarketValueByFund(1, null));
	}

	@Test
	public void getMarketValueByFund_basic_test_with_excludeHoldingList() throws InvalidAttributeValueException {
		// Adding Investors by passing Investor Name
		assertDoesNotThrow(() -> helper.addInvestor("Inv1"));

		// Adding Funds by passing Fund Name
		assertDoesNotThrow(() -> helper.addFund("F1"));
		assertDoesNotThrow(() -> helper.addFund("F2"));

		// Adding Holdings by passing Holding Name and Holding value
		assertDoesNotThrow(() -> helper.addHolding("H1", 10));
		assertDoesNotThrow(() -> helper.addHolding("H2", 20));
		assertDoesNotThrow(() -> helper.addHolding("H3", 15));
		assertDoesNotThrow(() -> helper.addHolding("H4", 10));

		// Adding Investor and Fund mapping by passing Investor Name, Fund Name, Level
		// and Quantity (NA in this case)
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F1", 1, 0));
		assertDoesNotThrow(() -> helper.addMappingByName("Inv1", "F2", 1, 0));

		// Adding Fund and Holding mapping by passing Fund Name, Holding Name, Level and
		// Quantity
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H2", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F1", "H4", 2, 100));

		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H1", 2, 100));
		assertDoesNotThrow(() -> helper.addMappingByName("F2", "H3", 2, 100));

		List<Integer> list = new ArrayList<>();
		list.add(1);

		assertEquals(3000, helper.getMarketValueByFund(1, list));

	}

	@Test
	public void getMarketValueByFund_basic_test_with_incorrect_investor_id() throws InvalidAttributeValueException {
		assertThrows(InvalidAttributeValueException.class, () -> helper.getMarketValueByFund(10, null));
	}
}
